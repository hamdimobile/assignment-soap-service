package com.example.soapdemo.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebConfig extends WsConfigurerAdapter {

  @Bean
  public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(
      ApplicationContext applicationContext) {
    MessageDispatcherServlet servlet = new MessageDispatcherServlet();
    servlet.setApplicationContext(applicationContext);
    servlet.setTransformWsdlLocations(true);
    return new ServletRegistrationBean<>(servlet, "/external/*");
  }

  // Define the DefaultWsdl11Definition bean
  @Bean("sampleServiceWsdl")
  public DefaultWsdl11Definition sampleServiceWsdl(@Qualifier("sampleServiceSchema") XsdSchema sampleServiceSchema) {
    DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
    wsdl11Definition.setPortTypeName("SampleServicePort");
    wsdl11Definition.setLocationUri("/external/services/ws/sample-service");
    wsdl11Definition.setTargetNamespace("http://www.oracle.com/external/services/sampleservice/request/v1.0");
    wsdl11Definition.setSchema(sampleServiceSchema);
    return wsdl11Definition;
  }

  // Define the XsdSchema bean
  @Bean("sampleServiceSchema")
  public XsdSchema sampleServiceSchema() {
    return new SimpleXsdSchema(new ClassPathResource("sample-service.xsd"));
  }
}
