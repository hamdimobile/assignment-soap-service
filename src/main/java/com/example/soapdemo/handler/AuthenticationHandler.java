package com.example.soapdemo.handler;

import java.util.Set;

import javax.xml.namespace.QName;

import jakarta.xml.soap.SOAPElement;
import jakarta.xml.soap.SOAPEnvelope;
import jakarta.xml.soap.SOAPException;
import jakarta.xml.soap.SOAPHeader;
import jakarta.xml.soap.SOAPHeaderElement;
import jakarta.xml.ws.handler.MessageContext;
import jakarta.xml.ws.handler.soap.SOAPHandler;
import jakarta.xml.ws.handler.soap.SOAPMessageContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AuthenticationHandler implements SOAPHandler<SOAPMessageContext> {

  @Override
  public Set<QName> getHeaders() {
    return null;
  }

  @Override
  public boolean handleMessage(SOAPMessageContext context) {
    Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

    if (Boolean.FALSE.equals(outbound)) {
      try {
        SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
        SOAPHeader header = envelope.getHeader();

        if (header == null) {
          throw new RuntimeException("Authentication header is missing");
        }

        SOAPHeaderElement authenticationHeader = (SOAPHeaderElement) header
            .getElementsByTagNameNS("http://www.oracle.com", "authenticationheader").item(0);

        if (authenticationHeader == null) {
          throw new RuntimeException("Authentication header is missing");
        }

        SOAPElement usernameElement = (SOAPElement) authenticationHeader
            .getElementsByTagNameNS("http://www.oracle.com", "username").item(0);
        SOAPElement passwordElement = (SOAPElement) authenticationHeader
            .getElementsByTagNameNS("http://www.oracle.com", "password").item(0);

        String username = usernameElement.getTextContent();
        String password = passwordElement.getTextContent();

        if (!validateCredentials(username, password)) {
          throw new RuntimeException("Invalid username or password");
        }

      } catch (SOAPException e) {
        e.printStackTrace();
      }
    }

    return true;
  }

  private boolean validateCredentials(String username, String password) {

    log.info("validateCredentials - username = {} | password = {}", username, password);

    return "YourUsername".equals(username) && "YourPassword".equals(password);
  }

  @Override
  public boolean handleFault(SOAPMessageContext context) {
    return true;
  }

  @Override
  public void close(MessageContext context) {
    // Clean up resources if needed
  }
}
