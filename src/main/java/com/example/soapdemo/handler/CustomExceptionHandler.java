package com.example.soapdemo.handler;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.soapdemo.exceptions.GeneralException;

@RestControllerAdvice
public class CustomExceptionHandler {

  private static final String ACCESS_DENIED_REASON = "access_denied_reason";

  @ExceptionHandler(Exception.class)
  public ProblemDetail handleSecurityException(Exception ex) {
    ProblemDetail errorDetail = null;
    if (ex instanceof AccessDeniedException) {
      errorDetail = ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(401), ex.getMessage());
      errorDetail.setProperty(ACCESS_DENIED_REASON, "not_authorized!");
    }

    if (ex instanceof IllegalStateException) {
      errorDetail = ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(400), ex.getMessage());
      errorDetail.setProperty(ACCESS_DENIED_REASON, ex.getMessage());
    }

    if (ex instanceof UserPrincipalNotFoundException) {
      errorDetail = ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(401), ex.getMessage());
      errorDetail.setProperty(ACCESS_DENIED_REASON, ex.getMessage());
    }

    if (ex instanceof SignatureException) {
      errorDetail = ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(403), ex.getMessage());
      errorDetail.setProperty(ACCESS_DENIED_REASON, "JWT Signature not valid");
    }

    if (ex instanceof IOException) {
      errorDetail = ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(500), ex.getMessage());
      errorDetail.setProperty(ACCESS_DENIED_REASON, "IO Exception");
    }

    return errorDetail;
  }

  @ExceptionHandler(GeneralException.class)
  public ProblemDetail handleGeneralException(GeneralException ex) {
    ProblemDetail errorDetail = ProblemDetail.forStatusAndDetail(ex.getHttpStatus(), ex.getMessage());
    errorDetail.setProperty(ACCESS_DENIED_REASON, ex.getMessage());
    return errorDetail;
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ProblemDetail handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
    ProblemDetail errorDetail = ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(400), ex.getMessage());

    Map<String, String> errorMap = new HashMap<>();
    ex.getBindingResult().getFieldErrors().forEach(error -> {
      errorMap.put(error.getField(), error.getDefaultMessage());
      errorDetail.setDetail(error.getDefaultMessage());
    });

    errorDetail.setProperty(ACCESS_DENIED_REASON, errorMap);
    return errorDetail;
  }
}