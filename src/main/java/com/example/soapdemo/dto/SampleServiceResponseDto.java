package com.example.soapdemo.dto;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

@XmlRootElement(name = "sampleservicers", namespace = "http://www.oracle.com/external/services/sampleservice/response/v1.0")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class SampleServiceResponseDto {

  @XmlElement(name = "error_code", namespace = "http://www.oracle.com/external/services/sampleservice/response/v1.0", required = true)
  private String errorCode;

  @XmlElement(name = "error_msg", namespace = "http://www.oracle.com/external/services/sampleservice/response/v1.0", required = true)
  private String errorMsg;

  @XmlElement(name = "trx_id", namespace = "http://www.oracle.com/external/services/sampleservice/response/v1.0")
  private String trxId;

  public static SampleServiceResponseDto from(RestServiceSampleResponseDto restResponse) {

    SampleServiceResponseDto dto = new SampleServiceResponseDto();
    dto.setErrorCode(restResponse.getSampleservicers().getErrorCode());
    dto.setErrorMsg(restResponse.getSampleservicers().getErrorMsg());
    dto.setTrxId(restResponse.getSampleservicers().getTrxId());
    return dto;

  }
}
