package com.example.soapdemo.dto;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@XmlRootElement(name = "authenticationheader", namespace = "http://www.oracle.com")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@ToString
public class AuthenticationHeader {

  public static final String NAMESPACE = "http://www.oracle.com";

  @XmlElement(namespace = "http://www.oracle.com", required = true)
  private String username;

  @XmlElement(namespace = "http://www.oracle.com", required = true)
  private String password;

}
