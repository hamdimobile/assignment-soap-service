package com.example.soapdemo.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

@XmlRootElement(name = "sampleservicerq", namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class SampleServiceRequestDto {

  @XmlElement(name = "service_id", namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0", required = true)
  @NotBlank(message = "service_id cannot be blank")
  private String serviceId;

  @XmlElement(name = "order_type", namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0", required = true)
  @NotBlank(message = "order_type cannot be blank")
  private String orderType;

  @XmlElement(name = "type", namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0")
  private String type;

  @XmlElement(name = "trx_id", namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0")
  private String trxId;

}
