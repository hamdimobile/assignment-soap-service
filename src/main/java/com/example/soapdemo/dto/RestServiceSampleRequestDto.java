package com.example.soapdemo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestServiceSampleRequestDto {

  @Valid
  @JsonProperty("sampleservicerq")
  private Sampleservicerq sampleservicerq;

  @Data
  public static class Sampleservicerq {

    @JsonProperty("service_id")
    @NotBlank(message = "service_id cannot be blank")
    private String serviceId;

    @JsonProperty("order_type")
    @NotBlank(message = "order_type cannot be blank")
    private String orderType;

    @JsonProperty("type")
    private String type;

    @JsonProperty("trx_id")
    private String trxId;

    @Override
    public String toString() {
      try {
        return new ObjectMapper().writeValueAsString(this);
      } catch (JsonProcessingException e) {
        e.printStackTrace();
        return null;
      }
    }
  }

  public static RestServiceSampleRequestDto from(SampleServiceRequestDto request) {

    Sampleservicerq sampleservicerq = new Sampleservicerq();
    sampleservicerq.setServiceId(request.getServiceId());
    sampleservicerq.setOrderType(request.getOrderType());
    sampleservicerq.setType(request.getType());
    sampleservicerq.setTrxId(request.getTrxId());

    return new RestServiceSampleRequestDto(sampleservicerq);
  }

  @Override
  public String toString() {
    try {
      return new ObjectMapper().writeValueAsString(this);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }

  
}
