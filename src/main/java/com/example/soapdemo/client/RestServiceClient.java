package com.example.soapdemo.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientResponseException;

import com.example.soapdemo.dto.RestServiceSampleRequestDto;
import com.example.soapdemo.dto.RestServiceSampleResponseDto;
import com.example.soapdemo.exceptions.GeneralException;

import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RestServiceClient {

  @Value("${application.config.rest-service.endpoint}")
  private String endpoint;

  private RestClient restClient;

  public RestServiceClient() {
    restClient = RestClient.builder().build();
  }

  public RestServiceSampleResponseDto restSampleService(@NotNull RestServiceSampleRequestDto request)
      throws GeneralException {
    RestServiceSampleResponseDto response = null;
    try {
      response = restClient.post() //
          .uri(endpoint) //
          .contentType(MediaType.APPLICATION_JSON) //
          .body(request) //
          .retrieve() //
          .body(RestServiceSampleResponseDto.class);

    } catch (RestClientResponseException e) {
      log.error("RestClientException -- ", e.getResponseBodyAsString());
      throw new GeneralException(e.getResponseBodyAsString(), HttpStatus.valueOf(e.getStatusCode().value()));

    } catch (Exception e) {
      log.error("Generic Exception", e);
      throw new GeneralException("Exception while calling Backend System", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return response;
  }

}
