package com.example.soapdemo.util;

import javax.xml.transform.dom.DOMSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.example.soapdemo.dto.AuthenticationHeader;
import com.example.soapdemo.exceptions.GeneralException;

@Component
public class AuthenticationUtilities {

  @Value("${application.config.soapheader.username:user}")
  private String username;

  @Value("${application.config.soapheader.password:password}")
  private String password;

  public AuthenticationHeader authenticationHeader(MessageContext messageContext) throws GeneralException {

    WebServiceMessage webServiceMessageRequest = messageContext.getRequest();
    SoapMessage soapMessage = (SoapMessage) webServiceMessageRequest;
    SoapHeader soapHdr = soapMessage.getSoapHeader();

    DOMSource bodyDomSource = (DOMSource) soapHdr.getSource();
    Node bodyNode = bodyDomSource.getNode();
    Node childnode = bodyNode.getChildNodes().item(1);
    NodeList nodeList = childnode.getChildNodes();

    AuthenticationHeader authenticationHeader = construct(nodeList);

    validate(authenticationHeader);

    return authenticationHeader;
  }

  private AuthenticationHeader construct(NodeList nodeList) {
    AuthenticationHeader authenticationHeader = new AuthenticationHeader();
    authenticationHeader.setUsername(nodeList.item(1).getTextContent());
    authenticationHeader.setPassword(nodeList.item(3).getTextContent());
    return authenticationHeader;
  }

  private void validate(AuthenticationHeader authenticationHeader) throws GeneralException {
    if (!username.equals(authenticationHeader.getUsername()) || !password.equals(authenticationHeader.getPassword())) {
      throw new GeneralException("Oops.. something wrong with Username or Password", HttpStatus.UNAUTHORIZED);
    }
  }

}
