package com.example.soapdemo.endpoint;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.example.soapdemo.dto.SampleServiceRequestDto;
import com.example.soapdemo.dto.SampleServiceResponseDto;
import com.example.soapdemo.exceptions.GeneralException;
import com.example.soapdemo.service.SampleService;
import com.example.soapdemo.util.AuthenticationUtilities;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@Component
@Endpoint
@Validated
@RequiredArgsConstructor
public class SampleEndpoint {

  private final SampleService sampleService;
  private final AuthenticationUtilities authenticationUtilities;

  @PayloadRoot(namespace = "http://www.oracle.com/external/services/sampleservice/request/v1.0", localPart = "sampleservicerq")
  @ResponsePayload
  public SampleServiceResponseDto handleSampleRequest(MessageContext messageContext,
      @Valid @RequestPayload SampleServiceRequestDto request) throws GeneralException {
    return sampleService.execute(authenticationUtilities.authenticationHeader(messageContext), request);
  }
}
