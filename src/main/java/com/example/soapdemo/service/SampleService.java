package com.example.soapdemo.service;

import com.example.soapdemo.dto.AuthenticationHeader;
import com.example.soapdemo.dto.SampleServiceRequestDto;
import com.example.soapdemo.dto.SampleServiceResponseDto;
import com.example.soapdemo.exceptions.GeneralException;

public interface SampleService {

  SampleServiceResponseDto execute(AuthenticationHeader authenticationHeader, SampleServiceRequestDto request) throws GeneralException;

}