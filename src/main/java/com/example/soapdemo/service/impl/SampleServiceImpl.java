package com.example.soapdemo.service.impl;

import org.springframework.stereotype.Service;

import com.example.soapdemo.client.RestServiceClient;
import com.example.soapdemo.dto.AuthenticationHeader;
import com.example.soapdemo.dto.RestServiceSampleRequestDto;
import com.example.soapdemo.dto.RestServiceSampleResponseDto;
import com.example.soapdemo.dto.SampleServiceRequestDto;
import com.example.soapdemo.dto.SampleServiceResponseDto;
import com.example.soapdemo.exceptions.GeneralException;
import com.example.soapdemo.service.SampleService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class SampleServiceImpl implements SampleService {

  private final RestServiceClient restServiceClient;

  @Override
  public SampleServiceResponseDto execute(AuthenticationHeader authenticationHeader, SampleServiceRequestDto request)
      throws GeneralException {

    validate(authenticationHeader, request);

    var req = constructRequest(request);

    var clientResponse = callClient(req);

    return constructResponse(clientResponse);
  }

  private void validate(AuthenticationHeader authenticationHeader, SampleServiceRequestDto request) {
    // some business validation ?
    log.info("AuthenticationHeader = {}", authenticationHeader);
    log.info("SampleServiceRequestDto = {}", request);
  }

  private RestServiceSampleRequestDto constructRequest(SampleServiceRequestDto request) {
    return RestServiceSampleRequestDto.from(request);
  }

  private SampleServiceResponseDto constructResponse(RestServiceSampleResponseDto restResponse) {
    return SampleServiceResponseDto.from(restResponse);
  }

  private RestServiceSampleResponseDto callClient(RestServiceSampleRequestDto restServiceSampleRequestDto) throws GeneralException {
    return restServiceClient.restSampleService(restServiceSampleRequestDto);
  }
}
