FROM eclipse-temurin:17

WORKDIR /opt/app
COPY ./target/soap-service.jar soap-service.jar
COPY ./src/main/resources/application.yml application.yml

EXPOSE 8080
ENV SOAPSERVICE_SERVER_PORT=8080
ENV RESTSERVICE_ENDPOINT=http://localhost:8081/external/services/rest/sample-service
ENV SOAPHEADER_USERNAME=user
ENV SOAPHEADER_PASSWORD=password

RUN date
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN date

ENTRYPOINT exec java -jar soap-service.jar
